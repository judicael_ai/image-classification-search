import { AlertColor } from "@mui/material";
import Alert from "@mui/material/Alert";
import Snackbar from "@mui/material/Snackbar";

interface INotificationProps {
  open: boolean;
  handleClose: () => void;
  title: string;
  type: AlertColor;
}

const Notification = ({
  open,
  handleClose,
  title,
  type,
}: INotificationProps) => {
  return (
    <Snackbar
      anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
      open={open}
      onClose={handleClose}
      key={1}
      autoHideDuration={6000}
    >
      <Alert severity={type}>{title}</Alert>
    </Snackbar>
  );
};

export default Notification;
