import Modal from "@mui/material/Modal";
import Box from "@mui/material/Box";

interface IImageModal {
  open: boolean;
  handleClose: () => void;
  image: string;
}

const ImageModal = ({ open, handleClose, image }: IImageModal) => {
  return (
    <Modal
      open={open}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box
        sx={{
          position: "absolute" as "absolute",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
          width: "60%",
          height: "60%",
          bgcolor: "background.paper",
          borderRadius: 2,
          boxShadow: 24,
          backgroundImage: `url(${image})`,
          backgroundRepeat: "no-repeat",
          backgroundSize: "contain",
          backgroundPosition: "center",
        }}
      ></Box>
    </Modal>
  );
};

export default ImageModal;
