import { createStyles, makeStyles } from "@mui/styles";

const useStyles = makeStyles(() =>
  createStyles({
    image: {
      width: "100%",
      height: "100%",
    },
  })
);

export default useStyles;
