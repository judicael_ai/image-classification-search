import { useMutation } from "react-query";

export const usePost = () => {
  const { mutate, data, isLoading, reset } = useMutation(
    async (value: string[] | string) => {
      const res = await fetch(process.env.NEXT_PUBLIC_BASE_URL as string, {
        method: "post",
        headers: {
          Authorization: process.env.NEXT_PUBLIC_TOKEN as string,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ input_data: { images: value } }),
      });
      return res.json();
    }
  );
  return { mutate, data, isLoading, reset };
};
