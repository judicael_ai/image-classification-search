export interface IClassify {
  labels: ILabel;
  image: string;
  value: string;
  rec: string[];
}

export interface ILabel {
  [key: string]: string;
}
