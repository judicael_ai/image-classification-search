import { useState, ChangeEvent } from "react";
import Image from "next/image";

import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import CircularProgress from "@mui/material/CircularProgress";
import Grid from "@mui/material/Grid";
import ImageList from "@mui/material/ImageList";
import ImageListItem from "@mui/material/ImageListItem";
import Input from "@mui/material/Input";
import Typography from "@mui/material/Typography";

import { usePost } from "@hooks/usePost";
import ImageModal from "@components/ImageModal";

import useStyles from "./style";

const Searching = () => {
  const [imageName, setImageName] = useState<string>("");
  const [itemName, setItemName] = useState<string>("");
  const [open, setOpen] = useState<boolean>(false);
  const { mutate, data, isLoading } = usePost();

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setImageName(event.target.value);
  };

  const handleSearch = async () => {
    mutate(imageName);
  };

  const classes = useStyles();
  return (
    <Box>
      <Grid
        container
        direction="row"
        justifyContent="space-between"
        alignItems="center"
      >
        <Typography>Search Image</Typography>
        <Input
          className={classes.searchInput}
          placeholder="Enter image name here..."
          value={imageName}
          onChange={handleChange}
        />
        <Button
          className={classes.searchBtn}
          onClick={handleSearch}
          disabled={!imageName || isLoading}
        >
          {isLoading ? (
            <CircularProgress color="inherit" size={24} />
          ) : (
            "Search"
          )}
        </Button>
      </Grid>
      <Grid>
        {data && data.images && data.images.length ? (
          <ImageList className={classes.imageList} cols={5} rowHeight={164}>
            {data.images.map((item: string) => (
              <ImageListItem
                onClick={() => {
                  setItemName(item);
                  setOpen(true);
                }}
                key={item}
                sx={{ cursor: "pointer" }}
              >
                <Image
                  src={`data:image/png;base64, ${item}`}
                  alt={`${imageName}`}
                  loading="lazy"
                  width={150}
                  height={150}
                />
              </ImageListItem>
            ))}
          </ImageList>
        ) : imageName && !data?.images.length ? (
          <Box
            display="flex"
            justifyContent="center"
            alignItems="center"
            className={classes.noImageWrapper}
          >
            <Typography>No image to show, Change the input search</Typography>
          </Box>
        ) : null}
      </Grid>
      <ImageModal
        open={open}
        handleClose={() => setOpen(false)}
        image={`data:image/png;base64,${itemName}`}
      />
    </Box>
  );
};

export default Searching;
