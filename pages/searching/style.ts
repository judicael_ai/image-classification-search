import { Theme } from "@mui/material";
import { createStyles, makeStyles } from "@mui/styles";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    searchInput: {
      width: "60%",
      border: "1px solid #e8e8e8",
      borderRadius: 4,
      height: 42,
      paddingLeft: "1rem",
      outline: "none",
      "&:after, :before": {
        borderBottom: "none",
      },
    },
    searchBtn: {
      width: 200,
      height: 42,
      background: theme.palette.primary.main,
      color: "white",
      fontWeight: theme.typography.fontWeightMedium,
      "&:hover": {
        background: theme.palette.primary.main,
      },
    },
    imageList: {
      width: "100%",
      height: 450,
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      gap: "8px",
    },
    noImageWrapper: {
      width: "100%",
      height: "200px",
    },
  })
);

export default useStyles;
