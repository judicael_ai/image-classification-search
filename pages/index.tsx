import { useState } from "react";
import type { NextPage } from "next";
import Head from "next/head";

import { Box, Container, Tab, Tabs } from "@mui/material";
import TabPanel from "@components/TabPanel";
import Searching from "./searching";
import Classify from "./classify";

function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

const Home: NextPage = () => {
  const [value, setValue] = useState(0);

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };
  return (
    <Box sx={{ width: "100%", height: "100vh", backgroundColor: "white" }}>
      <Head>
        <title>Image classification - searching</title>
      </Head>
      <Container maxWidth="lg">
        <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
          <Tabs
            value={value}
            onChange={handleChange}
            aria-label="basic tabs example"
          >
            <Tab label="Search" {...a11yProps(0)} />
            <Tab label="Classify" {...a11yProps(1)} />
          </Tabs>
        </Box>
        <TabPanel value={value} index={0}>
          <Searching />
        </TabPanel>
        <TabPanel value={value} index={1}>
          <Classify />
        </TabPanel>
      </Container>
    </Box>
  );
};

export default Home;
