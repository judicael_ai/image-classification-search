import { Theme } from "@mui/material";
import { createStyles, makeStyles } from "@mui/styles";

interface IClassifyProps {
  onDragOver: boolean;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    searchInput: {
      width: "100%",
      height: "100%",
      paddingLeft: "23%",
      border: "none",
      outline: "none",
      opacity: 0,
      cursor: "pointer",
      backgroundColor: "red",
      "&:after, :before": {
        borderBottom: "none",
      },
    },
    dropZone: {
      border: "1px dashed #4BB543",
      width: "60%",
      height: 42,
      borderRadius: 4,
      position: "relative",
      cursor: "pointer",
      transition: "transform 0.2s",
      transform: (props: IClassifyProps) =>
        props.onDragOver ? "scale(0.8)" : "scale(1)",
    },
    dropzoneLabel: {
      width: "100%",
      position: "absolute",
      marginLeft: "30rem",
      cursor: "pointer",
    },
    searchBtn: {
      width: 200,
      height: 42,
      background: theme.palette.primary.main,
      color: "white",
      fontWeight: theme.typography.fontWeightMedium,
      "&:hover": {
        background: theme.palette.primary.main,
      },
    },
    imagePreview: {
      marginLeft: "1rem",
    },
    arrowButton: {
      animation: `$shake 2s infinite ${theme.transitions.easing.easeInOut}`,
    },
    "@keyframes shake": {
      "0%": {
        transform: "translateX(15px)",
      },
      "50%": {
        transform: "translateX(50px)",
      },
      "100%": {
        transform: "translateX(20px)",
      },
    },
    similarTitle: {
      fontSize: theme.typography.pxToRem(16),
      color: "#191919",
      fontWeight: theme.typography.fontWeightMedium,
      padding: "4rem 0",
    },
    similarImage: {
      padding: "1rem",
    },
    removeButton: {
      cursor: "pointer",
      "&:hover": {
        color: "red",
      },
    },
  })
);

export default useStyles;
