import { ChangeEvent, useState, useEffect } from "react";
import Image from "next/image";

import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import ArrowRightAltIcon from "@mui/icons-material/ArrowRightAlt";
import CircularProgress from "@mui/material/CircularProgress";
import DeleteRoundedIcon from "@mui/icons-material/DeleteRounded";
import Tooltip from "@mui/material/Tooltip";

import { usePost } from "@hooks/usePost";
import Notification from "@components/Notification";
import { IClassify } from "types";
import ClassifyTip from "./components/ClassifyTip";

import useStyles from "./style";

const Classify = () => {
  const [showAlert, setShowAlert] = useState<boolean>(false);
  const [baseFiles, setbaseFiles] = useState<string[]>([]);
  const [fileList, setFileList] = useState<FileList>();
  const [classifyImage, setClassifyImage] = useState<any>();
  const [onDragOver, setOnDragOver] = useState<boolean>(false);
  const [classifiedData, setClassifiedData] = useState<IClassify[] | []>([]);
  const classes = useStyles({ onDragOver });
  const { mutate, data, isLoading, reset } = usePost();

  const getBase64 = async (file: File, callBack: any) => {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    // Function to execute after loading the file
    reader.onload = async () => {
      if (reader.result) {
        callBack(reader.result);
      }
    };
  };
  // Get the base 64 image from result
  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    event.preventDefault();
    const files = event.currentTarget.files;
    if (files) {
      if (files?.length > 5) {
        setShowAlert(true);
      } else {
        setClassifyImage(files);
        setFileList(files);
        Array.from(files).forEach((file) => {
          getBase64(file, (result: any) =>
            setbaseFiles((prev) => [...prev, ...[result]])
          );
        });
      }
    }
  };

  const handleDrop = (event: React.DragEvent<HTMLDivElement>) => {
    event.preventDefault();
    setOnDragOver(false);
    const files = event.dataTransfer.files;
    if (files) {
      if (files.length > 5) {
        setShowAlert(true);
      } else setFileList(files);
    }

    if (event.dataTransfer.items) {
      Array.from(event.dataTransfer.items).forEach((item) => {
        if (item.kind === "file") {
          let file: File | null = item.getAsFile();
          if (file) {
            getBase64(file, (result: any) =>
              setbaseFiles((prev) => [...prev, ...[result]])
            );
          }
        }
      });
    }
  };

  const handleDragOver = (event: React.DragEvent<HTMLDivElement>) => {
    event.preventDefault();
    setOnDragOver(true);
  };

  const handleDragEnd = (event: React.DragEvent<HTMLDivElement>) => {
    event.preventDefault();
    setOnDragOver(false);
  };

  const handleClassifyImages = () => {
    mutate(baseFiles);
  };

  useEffect(() => {
    if (data) setClassifiedData(data);
  }, [data]);

  const removeFiles = () => {
    setClassifyImage(undefined);
    setFileList(undefined);
    setbaseFiles([]);
    setClassifiedData([]);
    reset();
  };

  return (
    <Box>
      <Grid
        container
        direction="row"
        justifyContent="space-between"
        alignItems="center"
      >
        <Typography>Search Labels</Typography>
        <Box
          onDrop={handleDrop}
          onDragOver={handleDragOver}
          onDragLeave={handleDragEnd}
          display="flex"
          justifyContent="center"
          alignItems="center"
          className={classes.dropZone}
        >
          {fileList ? (
            <Box
              display="flex"
              justifyContent="center"
              alignItems="center"
              sx={{ width: "100%" }}
            >
              <Typography>
                {isLoading
                  ? "Classifying, please wait..."
                  : "Click the button to classify this images"}
              </Typography>
              {isLoading ? (
                <CircularProgress size={24} color="primary" />
              ) : (
                <ArrowRightAltIcon className={classes.arrowButton} />
              )}
            </Box>
          ) : (
            <>
              <Typography className={classes.dropzoneLabel}>
                <b>Browse</b> or drop an image
              </Typography>
              <input
                type="file"
                value={classifyImage}
                onChange={handleChange}
                className={classes.searchInput}
                multiple
              />
            </>
          )}
        </Box>
        {fileList && !isLoading && (
          <DeleteRoundedIcon
            className={classes.removeButton}
            onClick={removeFiles}
          />
        )}
        <Button
          className={classes.searchBtn}
          disabled={isLoading || !baseFiles || !classifyImage}
          onClick={handleClassifyImages}
        >
          {isLoading ? (
            <CircularProgress color="inherit" size={24} />
          ) : (
            "Classify"
          )}
        </Button>
      </Grid>
      <Grid container item direction="row" rowSpacing={2}>
        <Grid
          container
          item
          direction="row"
          rowSpacing={2}
          sx={{ marginTop: "2rem" }}
          justifyContent="center"
        >
          {fileList &&
            Array.from(fileList).map((file, index) => (
              <Box sx={{ padding: "1rem" }} key={file.name}>
                <Image
                  src={URL.createObjectURL(file)}
                  width={150}
                  height={150}
                  className={classes.imagePreview}
                />
                <Tooltip
                  title={
                    classifiedData && (
                      <ClassifyTip labels={classifiedData[index].labels} />
                    )
                  }
                >
                  <Typography>
                    {Object.keys(classifiedData[index].labels)[0]}
                  </Typography>
                </Tooltip>
              </Box>
            ))}
        </Grid>
      </Grid>
      {classifiedData && fileList && (
        <Box sx={{ marginTop: "1rem" }}>
          <Typography className={classes.similarTitle}>
            Similar images
          </Typography>
          <Grid
            container
            direction="column"
            justifyContent="space-around"
            sx={{ marginTop: -4 }}
          >
            {classifiedData.map((classified) => (
              <Grid
                container
                direction="row"
                alignItems="center"
                sx={{ marginTop: 2 }}
              >
                <Image
                  src={`data:image/png;base64,${classified.image}`}
                  width={150}
                  height={150}
                  className={classes.imagePreview}
                />
                <Typography sx={{ margin: "0 16px" }}>:</Typography>
                {classified.rec.map((similar: string) => (
                  <Grid
                    container
                    direction="row"
                    alignItems="center"
                    sx={{ overflowX: "auto", width: "auto" }}
                  >
                    <Image
                      src={`data:image/png;base64,${similar}`}
                      width={150}
                      height={150}
                      className={classes.similarImage}
                    />
                  </Grid>
                ))}
              </Grid>
            ))}
          </Grid>
        </Box>
      )}
      <Notification
        open={showAlert}
        handleClose={() => setShowAlert(!showAlert)}
        title="You can only upload five images"
        type="error"
      />
    </Box>
  );
};

export default Classify;
