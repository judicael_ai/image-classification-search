import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import { ILabel } from "types";

interface IClassifyTipProps {
  labels: ILabel;
}

const ClassifyTip = ({ labels }: IClassifyTipProps) => {
  return (
    <Box
      sx={{
        borderRadius: 4,
        padding: "1rem",
        backgroundColor: "#06040A",
        opacity: "53%",
        width: 100,
        height: "auto",
      }}
    >
      {Object.entries(labels).map(([key, value]) => (
        <Grid container direction="row" alignItems="center">
          <Typography>{key}</Typography>
          <Typography sx={{ marginLeft: 4 }}>{value}</Typography>
        </Grid>
      ))}
    </Box>
  );
};

export default ClassifyTip;
